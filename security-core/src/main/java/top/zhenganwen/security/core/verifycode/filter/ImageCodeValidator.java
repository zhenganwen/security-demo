package top.zhenganwen.security.core.verifycode.filter;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;
import top.zhenganwen.security.core.SecurityConstants;
import top.zhenganwen.security.core.verifycode.po.VerifyCode;

/**
 * @author zhenganwen
 * @date 2019/9/2
 * @desc ImageCodeValidator
 */
@Component
public class ImageCodeValidator extends VerifyCodeValidator {

    @Override
    protected void removeStoredVerifyCode(ServletWebRequest request) {
        sessionStrategy.removeAttribute(request, SecurityConstants.IMAGE_CODE_SESSION_KEY);
    }

    @Override
    protected VerifyCode getStoredVerifyCode(ServletWebRequest request) {
        return (VerifyCode) sessionStrategy.getAttribute(request, SecurityConstants.IMAGE_CODE_SESSION_KEY);
    }
}
