package top.zhenganwen.security.core.verifycode.constont;

/**
 * @author zhenganwen
 * @date 2019/8/29
 * @desc VerifyCodeConstant
 */
public class VerifyCodeConstant {
    public static final String IMAGE_CODE_SESSION_KEY = "SESSION_KEY_IMAGE_CODE";

    public static final String SMS_CODE_SESSION_KEY = "SESSION_KEY_SMS_CODE";

    public static final String VERIFY_CODE_PROCESSOR_IMPL_SUFFIX = "CodeProcessorImpl";

    public static final String VERIFY_CODE_Generator_IMPL_SUFFIX = "CodeGenerator";

    public static final String PHONE_NUMBER_PARAMETER_NAME = "phoneNumber";
}
