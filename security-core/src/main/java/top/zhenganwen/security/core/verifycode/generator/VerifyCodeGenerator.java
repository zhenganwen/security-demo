package top.zhenganwen.security.core.verifycode.generator;

import top.zhenganwen.security.core.verifycode.po.VerifyCode;

/**
 * @author zhenganwen
 * @date 2019/8/25
 * @desc VerifyCodeGenerator 验证码生成器接口
 */
public interface VerifyCodeGenerator<T extends VerifyCode> {

    /**
     * 生成验证码
     * @return
     */
    T generateVerifyCode();
}
