package top.zhenganwen.security.core.verifycode.sender;

/**
 * @author zhenganwen
 * @date 2019/8/28
 * @desc SmsCodeSender
 */
public interface SmsCodeSender {
    /**
     * 根据手机号发送短信验证码
     * @param smsCode
     * @param phoneNumber
     */
    void send(String smsCode, String phoneNumber);
}
