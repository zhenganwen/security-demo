package top.zhenganwen.security.core.verifycode.sender;

/**
 * @author zhenganwen
 * @date 2019/8/28
 * @desc DefaultSmsCodeSender
 */
public class DefaultSmsCodeSender implements SmsCodeSender {
    @Override
    public void send(String smsCode, String phoneNumber) {
        System.out.printf("向手机号%s发送短信验证码%s", phoneNumber, smsCode);
    }
}
