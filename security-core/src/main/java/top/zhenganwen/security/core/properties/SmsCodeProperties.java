package top.zhenganwen.security.core.properties;

import lombok.Data;

/**
 * @author zhenganwen
 * @date 2019/8/28
 * @desc SmsCodeProperties
 */
@Data
public class SmsCodeProperties {
    // 短信验证码数字个数，默认4个数字
    private int strLength = 4;
    // 有效时间，默认60秒
    private int durationSeconds = 60;

    private String uriPatterns;
}
