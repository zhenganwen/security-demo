package top.zhenganwen.security.core.social.qq.connect;

import lombok.Data;

/**
 * @author zhenganwen
 * @date 2019/9/5
 * @desc QQSecurityPropertie
 */
@Data
public class QQSecurityPropertie {
    private String appId;
    private String appSecret;
    private String providerId = "qq";
}
