package top.zhenganwen.security.core.verifycode.po;

import lombok.Data;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;

/**
 * @author zhenganwen
 * @date 2019/8/24
 * @desc ImageCode
 */
@Data
public class ImageCode extends VerifyCode{
    private BufferedImage image;
    public ImageCode(String code, BufferedImage image, LocalDateTime expireTime) {
        super(code,expireTime);
        this.image = image;
    }
    public ImageCode(String code, BufferedImage image, int durationSeconds) {
        this(code, image, LocalDateTime.now().plusSeconds(durationSeconds));
    }
}
