package top.zhenganwen.security.core.social.qq.connect;

import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;
import org.springframework.stereotype.Component;
import top.zhenganwen.security.core.social.qq.QQUserInfo;
import top.zhenganwen.security.core.social.qq.api.QQApiImpl;

/**
 * @author zhenganwen
 * @date 2019/9/4
 * @desc QQConnectionAdapter   从不同第三方应用返回的不同用户信息到统一用户视图{@link org.springframework.social.connect.Connection}的适配
 */
@Component
public class QQConnectionAdapter implements ApiAdapter<QQApiImpl> {

    // 测试OpenAPI接口是否可用
    @Override
    public boolean test(QQApiImpl api) {
        return true;
    }

    /**
     * 调用OpenAPI获取用户信息并适配成{@link org.springframework.social.connect.Connection}
     * 注意: 不是所有的社交应用都对应有{@link org.springframework.social.connect.Connection}中的属性，例如QQ就不像微博那样有个人主页
     * @param api
     * @param values
     */
    @Override
    public void setConnectionValues(QQApiImpl api, ConnectionValues values) {
        QQUserInfo userInfo = api.getUserInfo();
        // 用户昵称
        values.setDisplayName(userInfo.getNickname());
        // 用户头像
        values.setImageUrl(userInfo.getFigureurl_2());
        // 用户个人主页
        values.setProfileUrl(null);
        // 用户在社交平台上的id
        values.setProviderUserId(userInfo.getOpenId());
    }

    // 此方法作用和 setConnectionValues 类似，在后续开发社交账号绑定、解绑时再说
    @Override
    public UserProfile fetchUserProfile(QQApiImpl api) {
        return null;
    }

    /**
     * 调用OpenAPI更新用户动态
     * 由于QQ OpenAPI没有此功能，因此不用管(如果接入微博则可能需要重写此方法)
     * @param api
     * @param message
     */
    @Override
    public void updateStatus(QQApiImpl api, String message) {

    }
}
