package top.zhenganwen.security.core.properties;

import lombok.Data;

/**
 * @author zhenganwen
 * @date 2019/8/25
 * @desc ImageCodeProperties
 */
@Data
public class ImageCodeProperties extends SmsCodeProperties{
    private int width=67;
    private int height=23;
    private String uriPatterns;

    public ImageCodeProperties() {
        // 图形验证码默认显示6个字符
        this.setStrLength(6);
        // 图形验证码过期时间默认为3分钟
        this.setDurationSeconds(180);
    }
}
