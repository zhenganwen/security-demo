package top.zhenganwen.security.core.verifycode.processor;

import org.springframework.web.context.request.ServletWebRequest;
import top.zhenganwen.security.core.verifycode.po.VerifyCode;

/**
 * @author zhenganwen
 * @date 2019/8/29
 * @desc AbstractVerifyCodeProcessor
 */
public abstract class AbstractVerifyCodeProcessor<T extends VerifyCode> implements VerifyCodeProcessor {

    @Override
    public void sendVerifyCode(ServletWebRequest request) {
        T verifyCode = generateVerifyCode(request);
        save(request, verifyCode);
        send(request, verifyCode);
    }

    /**
     * 生成验证码
     *
     * @param request
     * @return
     */
    public abstract T generateVerifyCode(ServletWebRequest request);

    /**
     * 保存验证码
     *
     * @param request
     * @param verifyCode
     */
    public abstract void save(ServletWebRequest request, T verifyCode);

    /**
     * 发送验证码
     *
     * @param request
     * @param verifyCode
     */
    public abstract void send(ServletWebRequest request, T verifyCode);
}
