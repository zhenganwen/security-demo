package top.zhenganwen.security.core.facade;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhenganwen
 * @date 2019/8/23
 * @desc SimpleResponseResult
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SimpleResponseResult {
    private Object content;
}
