package top.zhenganwen.security.core.social.qq;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhenganwen
 * @date 2019/9/4
 * @desc QQUserInfo 用户在QQ应用注册的信息
 */
@Data
public class QQUserInfo implements Serializable {

    /**
     * ret : 0
     * msg :
     * is_lost : 0
     * nickname : Anwen
     * gender : 男
     * province : 湖北
     * city : 黄石
     * year : 1998
     * constellation :
     * figureurl : http://qzapp.qlogo.cn/qzapp/101781017/00149A9EE9E4AD09F7DDA3DC7D9F0523/30
     * figureurl_1 : http://qzapp.qlogo.cn/qzapp/101781017/00149A9EE9E4AD09F7DDA3DC7D9F0523/50
     * figureurl_2 : http://qzapp.qlogo.cn/qzapp/101781017/00149A9EE9E4AD09F7DDA3DC7D9F0523/100
     * figureurl_qq_1 : http://thirdqq.qlogo.cn/g?b=oidb&k=Ct8QbJQIaiaTiaNfpj9IMAEw&s=40&t=1565414944
     * figureurl_qq_2 : http://thirdqq.qlogo.cn/g?b=oidb&k=Ct8QbJQIaiaTiaNfpj9IMAEw&s=100&t=1565414944
     * figureurl_qq : http://thirdqq.qlogo.cn/g?b=oidb&k=Ct8QbJQIaiaTiaNfpj9IMAEw&s=140&t=1565414944
     * figureurl_type : 1
     * is_yellow_vip : 0
     * vip : 0
     * yellow_vip_level : 0
     * level : 0
     * is_yellow_year_vip : 0
     */

    private int ret;
    private String msg;
    private int is_lost;
    private String nickname;
    private String gender;
    private String province;
    private String city;
    private String year;
    private String constellation;
    private String figureurl;
    private String figureurl_1;
    private String figureurl_2;
    private String figureurl_qq_1;
    private String figureurl_qq_2;
    private String figureurl_qq;
    private String figureurl_type;
    private String is_yellow_vip;
    private String vip;
    private String yellow_vip_level;
    private String level;
    private String is_yellow_year_vip;

    /**
     * 用户在社交系统上的id
     */
    private String openId;
}
