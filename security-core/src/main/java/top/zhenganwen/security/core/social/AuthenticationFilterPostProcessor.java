package top.zhenganwen.security.core.social;

import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

/**
 * @author zhenganwen
 * @date 2019/9/15
 * @desc 认证过滤器后置处理器
 */
public interface AuthenticationFilterPostProcessor<T extends AbstractAuthenticationProcessingFilter> {
    /**
     * 对认证过滤器做一个增强，例如替换默认的认证成功处理器等
     * @param filter
     */
    void process(T filter);
}
