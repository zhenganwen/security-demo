package top.zhenganwen.security.core.verifycode.sms;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Objects;

/**
 * @author zhenganwen
 * @date 2019/8/30
 * @desc SmsUserDetailsService
 */
public class SmsUserDetailsService implements UserDetailsService {

    /**
     * 根据登录名查询用户，这里登录名是手机号
     *
     * @param phoneNumber
     * @return
     * @throws PhoneNumberNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String phoneNumber) throws PhoneNumberNotFoundException {
        // 实际上应该调用DAO根据手机号查询用户
        if (Objects.equals(phoneNumber, "12345678912") == false) {
            // 未查到
            throw new PhoneNumberNotFoundException();
        }
        // 查到了
        // 为了方便你可以将你的User实体类实现UserDetails接口，这样就可以直接返回DAO查出来的对象了
        return new User("anwen","123456", AuthorityUtils.createAuthorityList("admin","super_admin"));
    }
}
