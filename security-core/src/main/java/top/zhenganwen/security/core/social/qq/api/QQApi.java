package top.zhenganwen.security.core.social.qq.api;

import top.zhenganwen.security.core.social.qq.QQUserInfo;

/**
 * @author zhenganwen
 * @date 2019/9/4
 * @desc QQApi  封装对QQ开放平台接口的调用
 */
public interface QQApi {

    QQUserInfo getUserInfo();
}
