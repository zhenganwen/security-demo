package top.zhenganwen.security.core.properties;

import lombok.Data;

/**
 * @author zhenganwen
 * @date 2019/8/23
 * @desc BrowserProperties  封装security-browser模块的配置项
 */
@Data
public class BrowserProperties {
    private String loginPage = "/sign-in.html";
    //默认返回JSON信息
    private LoginProcessTypeEnum loginProcessType = LoginProcessTypeEnum.JSON;
}
