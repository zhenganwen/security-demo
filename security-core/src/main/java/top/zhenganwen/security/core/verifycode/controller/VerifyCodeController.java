package top.zhenganwen.security.core.verifycode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;
import top.zhenganwen.security.core.verifycode.processor.VerifyCodeProcessor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static top.zhenganwen.security.core.verifycode.constont.VerifyCodeConstant.VERIFY_CODE_PROCESSOR_IMPL_SUFFIX;
import static top.zhenganwen.security.core.verifycode.constont.VerifyCodeTypeEnum.IMAGE;
import static top.zhenganwen.security.core.verifycode.constont.VerifyCodeTypeEnum.SMS;

/**
 * @author zhenganwen
 * @date 2019/8/24
 * @desc VerifyCodeController
 */
@RestController
@RequestMapping("/verifyCode")
public class VerifyCodeController {

/*    private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();

    @Autowired
    private VerifyCodeGenerator<ImageCode> imageCodeGenerator;

    @Autowired
    private VerifyCodeGenerator<VerifyCode> smsCodeGenerator;

    @Autowired
    private SmsCodeSender smsCodeSender;

    *//**
     * 1.生成图形验证码
     * 2.将验证码存到session中
     * 3.将图形响应给前端
     *//*
    @GetMapping("/image")
    public void imageCode(HttpServletRequest request, HttpServletResponse response) throws IOException {

        ImageCode imageCode = imageCodeGenerator.generateVerifyCode();
        sessionStrategy.setAttribute(new ServletWebRequest(request), IMAGE_CODE_SESSION_KEY, imageCode);
        ImageIO.write(imageCode.getImage(), "JPEG", response.getOutputStream());
    }

    *//**
     * 1.生成短信验证码
     * 2.将验证码存到session中
     * 3.调用短信验证码发送器发送短信
     *//*
    @GetMapping("/sms")
    public void smsCode(HttpServletRequest request, HttpServletResponse response) throws ServletRequestBindingException {
        long phoneNumber = ServletRequestUtils.getRequiredLongParameter(request, "phoneNumber");
        VerifyCode verifyCode = smsCodeGenerator.generateVerifyCode();
        sessionStrategy.setAttribute(new ServletWebRequest(request), SMS_CODE_SESSION_KEY, verifyCode);
        smsCodeSender.send(verifyCode.getCode(), String.valueOf(phoneNumber));
    }*/

    @Autowired
    private Map<String, VerifyCodeProcessor> verifyCodeProcessorMap = new HashMap<>();

    @GetMapping("/{type}")
    public void sendVerifyCode(@PathVariable String type, HttpServletRequest request, HttpServletResponse response) {
        if (Objects.equals(type, IMAGE.getType()) == false && Objects.equals(type, SMS.getType()) == false) {
            throw new IllegalArgumentException("不支持的验证码类型");
        }
        VerifyCodeProcessor verifyCodeProcessor = verifyCodeProcessorMap.get(type + VERIFY_CODE_PROCESSOR_IMPL_SUFFIX);
        verifyCodeProcessor.sendVerifyCode(new ServletWebRequest(request, response));
    }
}
