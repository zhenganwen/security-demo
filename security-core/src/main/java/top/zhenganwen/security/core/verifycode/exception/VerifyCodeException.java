package top.zhenganwen.security.core.verifycode.exception;


import org.springframework.security.core.AuthenticationException;

/**
 * @author zhenganwen
 * @date 2019/8/24
 * @desc VerifyCodeException
 */
public class VerifyCodeException extends AuthenticationException {
    public VerifyCodeException(String explanation) {
        super(explanation);
    }
}
