package top.zhenganwen.security.core.verifycode.constont;

/**
 * @author zhenganwen
 * @date 2019/8/29
 * @desc VerifyCodeTypeEnum
 */
public enum VerifyCodeTypeEnum {

    IMAGE("image"),SMS("sms");

    private String type;

    public String getType() {
        return type;
    }

    VerifyCodeTypeEnum(String type) {
        this.type = type;
    }
}
