package top.zhenganwen.security.core.verifycode.filter;

import top.zhenganwen.security.core.SecurityConstants;

/**
 * @author zhenganwen
 * @date 2019/9/2
 * @desc VerifyCodeType
 */
public enum VerifyCodeType {

    SMS{
        @Override
        public String getVerifyCodeParameterName() {
            return SecurityConstants.DEFAULT_SMS_CODE_PARAMETER_NAME;
        }
    },

    IMAGE{
        @Override
        public String getVerifyCodeParameterName() {
            return SecurityConstants.DEFAULT_IMAGE_CODE_PARAMETER_NAME;
        }
    };

    public abstract String getVerifyCodeParameterName();
}
