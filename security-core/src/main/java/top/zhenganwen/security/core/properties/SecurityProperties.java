package top.zhenganwen.security.core.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
* @author zhenganwen
* @date 2019/8/23
* @desc SecurityProperties 封装整个项目各模块的配置项
*/
@Data
@ConfigurationProperties(prefix = "demo.security")
public class SecurityProperties {
    private BrowserProperties browser = new BrowserProperties();
    private VerifyCodeProperties code = new VerifyCodeProperties();
    private SocialProperties social = new SocialProperties();
}
