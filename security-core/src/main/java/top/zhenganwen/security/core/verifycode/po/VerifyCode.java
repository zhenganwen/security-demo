package top.zhenganwen.security.core.verifycode.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author zhenganwen
 * @date 2019/8/28
 * @desc VerifyCode
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VerifyCode implements Serializable {
    protected String code;
    protected LocalDateTime expireTime;
    public boolean isExpired() {
        return LocalDateTime.now().isAfter(expireTime);
    }
}
