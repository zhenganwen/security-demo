package top.zhenganwen.security.core.verifycode.sms;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;

import java.util.Collection;

/**
 * @author zhenganwen
 * @date 2019/8/30
 * @desc SmsAuthenticationToken
 */
public class SmsAuthenticationToken extends AbstractAuthenticationToken {

    private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

    // ~ Instance fields
    // ================================================================================================
    // 认证前保存的是用户输入的手机号，认证成功后保存的是后端存储的用户详情
    private final Object principal;

    // ~ Constructors
    // ===================================================================================================

    /**
     * 认证前时调用该方法封装请求参数成一个未认证的token => authRequest
     *
     * @param phoneNumber 手机号
     */
    public SmsAuthenticationToken(Object phoneNumber) {
        super(null);
        this.principal = phoneNumber;
        setAuthenticated(false);
    }

    /**
     * 认证成功后需要调用该方法封装用户信息成一个已认证的token => successToken
     *
     * @param principal   用户详情
     * @param authorities 权限信息
     */
    public SmsAuthenticationToken(Object principal, Object credentials,
                                  Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.principal = principal;
        super.setAuthenticated(true); // must use super, as we override
    }

    // ~ Methods
    // ========================================================================================================

    // 用户名密码登录的凭证是密码，验证码登录不传密码
    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return this.principal;
    }

}
