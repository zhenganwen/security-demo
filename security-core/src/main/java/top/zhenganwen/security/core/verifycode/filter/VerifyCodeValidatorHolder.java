package top.zhenganwen.security.core.verifycode.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.zhenganwen.security.core.SecurityConstants;
import top.zhenganwen.security.core.verifycode.exception.VerifyCodeException;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author zhenganwen
 * @date 2019/9/2
 * @desc VerifyCodeProcessor
 */
@Component
public class VerifyCodeValidatorHolder {

    @Autowired
    private Map<String, VerifyCodeValidator> verifyCodeValidatorMap = new HashMap<>();

    public VerifyCodeValidator getVerifyCodeValidator(VerifyCodeType verifyCodeType) {
        VerifyCodeValidator verifyCodeValidator =
                verifyCodeValidatorMap.get(verifyCodeType.toString().toLowerCase() + SecurityConstants.VERIFY_CODE_VALIDATOR_NAME_SUFFIX);
        if (Objects.isNull(verifyCodeType)) {
            throw new VerifyCodeException("不支持的验证码类型:" + verifyCodeType);
        }
        return verifyCodeValidator;
    }

}
