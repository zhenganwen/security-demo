package top.zhenganwen.security.core.verifycode.sms;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author zhenganwen
 * @date 2019/8/30
 * @desc PhoneNumberNotFoundException   继承登录名未找到异常
 */
public class PhoneNumberNotFoundException extends UsernameNotFoundException {

    public static final String DEFAULT_MESSAGE = "电话号码不存在";

    public PhoneNumberNotFoundException() {
        super(DEFAULT_MESSAGE);
    }

    public PhoneNumberNotFoundException(Throwable t) {
        super(DEFAULT_MESSAGE, t);
    }
}
