package top.zhenganwen.security.core.verifycode.generator;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.zhenganwen.security.core.properties.SecurityProperties;
import top.zhenganwen.security.core.verifycode.po.VerifyCode;

import java.time.LocalDateTime;

/**
 * @author zhenganwen
 * @date 2019/8/28
 * @desc SmsCodeGenerator
 */
@Component("smsCodeGenerator")
public class SmsCodeGenerator implements VerifyCodeGenerator<VerifyCode> {

    @Autowired
    private SecurityProperties securityProperties;

    @Override
    public VerifyCode generateVerifyCode() {
        // 随机生成一串纯数字字符串，数字个数为 strLength
        String randomCode = RandomStringUtils.randomNumeric(securityProperties.getCode().getSms().getStrLength());
        return new VerifyCode(randomCode, LocalDateTime.now().plusSeconds(securityProperties.getCode().getSms().getDurationSeconds()));
    }

}
