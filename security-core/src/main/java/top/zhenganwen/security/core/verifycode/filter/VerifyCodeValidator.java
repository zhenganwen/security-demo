package top.zhenganwen.security.core.verifycode.filter;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.web.HttpSessionSessionStrategy;
import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;
import top.zhenganwen.security.core.verifycode.exception.VerifyCodeException;
import top.zhenganwen.security.core.verifycode.po.VerifyCode;

import java.util.Objects;

/**
 * @author zhenganwen
 * @date 2019/9/2
 * @desc VerifyCodeValidator
 */
public abstract class VerifyCodeValidator {

    protected SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();

    @Autowired
    private VerifyCodeValidatorHolder verifyCodeValidatorHolder;

    /**
     * 校验验证码
     * 1.从请求中获取传入的验证码
     * 2.从服务端获取存储的验证码
     * 3.校验验证码
     * 4.校验成功移除服务端验证码，校验失败抛出异常信息
     *
     * @param request
     * @param verifyCodeType
     * @throws VerifyCodeException
     */
    public void validateVerifyCode(ServletWebRequest request, VerifyCodeType verifyCodeType) throws VerifyCodeException {
        String requestCode = getVerifyCodeFromRequest(request, verifyCodeType);

        VerifyCodeValidator codeValidator = verifyCodeValidatorHolder.getVerifyCodeValidator(verifyCodeType);
        if (Objects.isNull(codeValidator)) {
            throw new VerifyCodeException("不支持的验证码校验类型: " + verifyCodeType);
        }
        VerifyCode storedVerifyCode = codeValidator.getStoredVerifyCode(request);

        codeValidator.validate(requestCode, storedVerifyCode);

        codeValidator.removeStoredVerifyCode(request);
    }

    /**
     * 校验验证码是否过期，默认进行简单的文本比对，子类可重写以校验传入的明文验证码和后端存储的密文验证码
     *
     * @param requestCode
     * @param storedVerifyCode
     */
    private void validate(String requestCode, VerifyCode storedVerifyCode) {
        if (Objects.isNull(storedVerifyCode) || storedVerifyCode.isExpired()) {
            throw new VerifyCodeException("验证码已失效，请重新生成");
        }
        if (StringUtils.isBlank(requestCode)) {
            throw new VerifyCodeException("验证码不能为空");
        }
        if (StringUtils.equalsIgnoreCase(requestCode, storedVerifyCode.getCode()) == false) {
            throw new VerifyCodeException("验证码错误");
        }
    }

    /**
     * 是从Session中还是从其他缓存方式移除验证码由子类自己决定
     *
     * @param request
     */
    protected abstract void removeStoredVerifyCode(ServletWebRequest request);

    /**
     * 是从Session中还是从其他缓存方式读取验证码由子类自己决定
     *
     * @param request
     * @return
     */
    protected abstract VerifyCode getStoredVerifyCode(ServletWebRequest request);


    /**
     * 默认从请求中获取验证码参数，可被子类重写
     *
     * @param request
     * @param verifyCodeType
     * @return
     */
    private String getVerifyCodeFromRequest(ServletWebRequest request, VerifyCodeType verifyCodeType) {
        try {
            return ServletRequestUtils.getStringParameter(request.getRequest(), verifyCodeType.getVerifyCodeParameterName());
        } catch (ServletRequestBindingException e) {
            throw new VerifyCodeException("非法请求，请附带验证码参数");
        }
    }

}
