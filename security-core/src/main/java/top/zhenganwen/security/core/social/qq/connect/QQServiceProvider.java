package top.zhenganwen.security.core.social.qq.connect;

import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;
import org.springframework.social.oauth2.OAuth2Operations;
import top.zhenganwen.security.core.social.qq.api.QQApiImpl;

/**
 * @author zhenganwen
 * @date 2019/9/4
 * @desc QQServiceProvider 对接服务提供商，封装一整套授权登录流程, 从用户点击第三方登录按钮到掉第三方应用OpenAPI获取Connection(用户信息)
 * 委托 {@link OAuth2Operations} 和 {@link org.springframework.social.oauth2.AbstractOAuth2ApiBinding}来完成整个流程
 */
public class QQServiceProvider extends AbstractOAuth2ServiceProvider<QQApiImpl> {

    /**
     * 当前应用在服务提供商注册的应用id
     */
    private String appId;

    /**
     * @param oauth2Operations 封装逻辑: 跳转到认证服务器、用户授权、获取授权码、获取token
     * @param appId            当前应用的appId
     */
    public QQServiceProvider(OAuth2Operations oauth2Operations, String appId) {
        super(oauth2Operations);
        this.appId = appId;
    }

    @Override
    public QQApiImpl getApi(String accessToken) {
        return new QQApiImpl(accessToken,appId);
    }
}
