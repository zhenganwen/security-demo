package top.zhenganwen.securitydemo.web.controller;

import org.springframework.social.connect.web.HttpSessionSessionStrategy;
import org.springframework.social.connect.web.ProviderSignInAttempt;
import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zhenganwen
 * @date 2019/9/6
 * @desc SocialController
 */
@RestController
@RequestMapping("/social")
public class SocialController {

    private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();

    @GetMapping
    public ProviderSignInAttempt socialInfo(HttpServletRequest request) {
        return getConnectionFromSession(request);
    }

    private ProviderSignInAttempt getConnectionFromSession(HttpServletRequest request) {
        ProviderSignInAttempt providerSignInAttempt = (ProviderSignInAttempt) sessionStrategy.getAttribute(new ServletWebRequest(request),
                ProviderSignInAttempt.SESSION_ATTRIBUTE);
        return providerSignInAttempt;
    }
}
