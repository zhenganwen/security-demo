package top.zhenganwen.securitydemo.web.async;

import org.springframework.stereotype.Component;

import java.util.LinkedList;

/**
 * @author zhenganwen
 * @date 2019/8/22
 * @desc OrderProcessingQueue   下单消息MQ
 */
@Component
public class OrderProcessingQueue extends LinkedList<String> {
}
