package top.zhenganwen.securitydemo.security;

import top.zhenganwen.security.core.verifycode.generator.VerifyCodeGenerator;
import top.zhenganwen.security.core.verifycode.po.ImageCode;

/**
 * @author zhenganwen
 * @date 2019/8/25
 * @desc CustomImageCodeGenerator
 */
//@Component
public class CustomImageCodeGenerator implements VerifyCodeGenerator<ImageCode> {
    @Override
    public ImageCode generateVerifyCode() {
        System.out.println("调用自定义的代码生成器");
        return null;
    }
}
