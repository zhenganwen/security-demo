package top.zhenganwen.securitydemo.dto;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import top.zhenganwen.securitydemo.annotation.valid.Unrepeatable;

import javax.validation.constraints.Past;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zhenganwen
 * @date 2019/8/18
 * @desc User
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    /**
     * 普通视图，返回用户基本信息
     */
    public interface UserBasicView {

    }

    /**
     * 详情视图，除了普通视图包含的字段，还返回密码等详细信息
     */
    public interface UserDetailsView extends UserBasicView {

    }

    private Long id;

    @Unrepeatable
    @NotBlank(message = "用户名不能为空")
    @JsonView(UserBasicView.class)
    private String username;

    @NotBlank(message = "密码不能为空")
    @JsonView(UserDetailsView.class)
    private String password;

    @Past(message = "生日必须是过去的一个时间点")
    private Date birthday;
}
