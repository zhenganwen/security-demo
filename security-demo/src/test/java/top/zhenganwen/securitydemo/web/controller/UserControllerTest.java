package top.zhenganwen.securitydemo.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author zhenganwen
 * @date 2019/8/18
 * @desc UserControllerTest
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void query() throws Exception {
        mockMvc.perform(get("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("username", "tom"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(3));
    }

    @Test
    public void testBadRequest() throws Exception {
        mockMvc.perform(get("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testParamBind() throws Exception {
        mockMvc.perform(get("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("username", "tom"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(3));
    }

    @Test
    public void testDefaultValue() throws Exception {
        mockMvc.perform(get("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(3));
    }

    @Test
    public void testForceBind() throws Exception {
        mockMvc.perform(get("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("username", "tom"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(3));
    }

    @Test
    public void testDtoBind() throws Exception {
        mockMvc.perform(get("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("username", "tom")
                .param("password", "123456")
                .param("phone", "12345678911"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(3));
    }

    @Test
    public void testPageable() throws Exception {
        mockMvc.perform(get("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("username", "tom")
                .param("password", "123456")
                .param("phone", "12345678911")
                .param("page", "2")
                .param("size", "30")
                .param("sort", "age,desc"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(3));
    }

    @Test
    public void testPathVariable() throws Exception {
        mockMvc.perform(get("/user/1").
                contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value("jack"));
    }

    @Test
    public void testRegExSuccess() throws Exception {
        mockMvc.perform(get("/user/1").
                contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    public void testRegExFail() throws Exception {
        mockMvc.perform(get("/user/abc").
                contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testUserBasicViewSuccess() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();
        System.out.println(mvcResult.getResponse().getContentAsString());
    }

    @Test
    public void testUserDetailsViewSuccess() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/user/1").
                contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();
        System.out.println(mvcResult.getResponse().getContentAsString());
    }

    @Test
    public void testCreateUser() throws Exception {
        mockMvc.perform(post("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"username\":\"jack\",\"password\":\"123\"}"))
                .andExpect(status().isOk());
    }

    @Test
    public void testDateBind() throws Exception {
        Date date = new Date();
        System.out.println(date.getTime());
        mockMvc.perform(post("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"username\":\"jack\",\"password\":\"123\",\"birthday\":\"" + date.getTime() + "\"}"))
                .andExpect(status().isOk());
    }

    @Test
    public void testConstraintValidateFail() throws Exception {
        mockMvc.perform(post("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"username\":\"\"}"))
                .andExpect(status().isOk());
    }

    @Test
    public void testConstraintValidateSuccess() throws Exception {
        mockMvc.perform(post("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"username\":\"\"}"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testValidatePastTimeSuccess() throws Exception {
        // 获取一年前的时间点
        Date date = new Date(LocalDateTime.now().plusYears(-1).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        mockMvc.perform(post("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"username\":\"jack\",\"password\":\"123\",\"birthday\":\"" + date.getTime() + "\"}"))
                .andExpect(status().isOk());
    }

    @Test
    public void testValidatePastTimeFail() throws Exception {
        // 获取一年后的时间点
        Date date = new Date(LocalDateTime.now().plusYears(1).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        mockMvc.perform(post("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"username\":\"jack\",\"password\":\"123\",\"birthday\":\"" + date.getTime() + "\"}"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testUpdateFail() throws Exception {
        mockMvc.perform(put("/user/1").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"username\":\"jack\",\"password\":\" \"}"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testUpdateSuccess() throws Exception {
        mockMvc.perform(put("/user/1").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"username\":\"jack\",\"password\":\"789\"}"))
                .andExpect(status().isOk());
    }

    @Test
    public void testBindingResult() throws Exception {
        Date date = new Date(LocalDateTime.now().plusYears(-1).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        mockMvc.perform(post("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"username\":\"jack\",\"password\":null,\"birthday\":\"" + date.getTime() + "\"}"))
                .andExpect(status().isOk());
    }

    @Test
    public void testBindingResult2() throws Exception {
        Date date = new Date(LocalDateTime.now().plusYears(-1).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        mockMvc.perform(put("/user/1").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"username\":\"jack\",\"password\":null,\"birthday\":\"" + date.getTime() + "\"}"))
                .andExpect(status().isOk());
    }

    @Test
    public void testBindingResult3() throws Exception {
        Date date = new Date(LocalDateTime.now().plusYears(-1).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        mockMvc.perform(put("/user/1").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"username\":\" \",\"password\":null,\"birthday\":\"" + date.getTime() + "\"}"))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateUserWithNewUsername() throws Exception {
        Date date = new Date(LocalDateTime.now().plusYears(-1).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        mockMvc.perform(post("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"username\":\"alice\",\"password\":\"123\",\"birthday\":\"" + date.getTime() + "\"}"))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateUserWithExistedUsername() throws Exception {
        Date date = new Date(LocalDateTime.now().plusYears(-1).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        mockMvc.perform(post("/user").
                contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"username\":\"tom\",\"password\":\"123\",\"birthday\":\"" + date.getTime() + "\"}"))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteUser() throws Exception {
        mockMvc.perform(delete("/user/1").
                contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    public void testDefaultExceptionResult() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/user/1").
                contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        System.out.println(mvcResult);
    }
}